package com.example.games.cifra_de_cesar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    public static final char[] ALFABETO = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    public static  final int ALTERADOR = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void cifrar (View v) {
        this.codificador(ALFABETO);
    }

    public void decifrar (View v) {
        this.codificador(this.inverter_alfabeto());
    }

    private void codificador (char[] alfabeto) {
        EditText texto_entrada = (EditText) findViewById(R.id.texto_entrada);
        char[] chars_entrada =  texto_entrada.getText().toString().toCharArray();

        String resultado = "";
        int tamanho_alfabeto = alfabeto.length;

        for (int i = 0; i < chars_entrada.length; i++) {
            if (chars_entrada[i] != ' ') {
                for (int j = 0; j < tamanho_alfabeto; j++) {
                    if (chars_entrada[i] == alfabeto[j]) {
                        resultado += String.valueOf(alfabeto[(j + ALTERADOR) % tamanho_alfabeto]);
                        break;
                    }
                }
            } else {
                resultado += " ";
            }
        }

        TextView texto_saida = (TextView) findViewById(R.id.texto_saida);
        texto_saida.setText(resultado);
    }

    private char[] inverter_alfabeto () {
        char[] alfabeto_reverso = new char[ALFABETO.length];
        int contador = 0;
        for (int i = ALFABETO.length-1; i >= 0 ; i--){
            alfabeto_reverso[contador] = ALFABETO[i];
            contador++;
        }
        return alfabeto_reverso;
    }
}
